from random import randint
from time import time

def bruteforce(key: int) -> float:
    start = time()

    candidate = 0
    while candidate != key:
        candidate += 1

    end = time()

    return round((end - start) * 1000.0, 0)

def number_of_keys(bits: int) -> int:
    if bits <= 0:
        return 0
    else:
        return 2**bits

def random_keys(bits_bounds: list) -> list:
    return [randint(0, number_of_keys(bits)) for bits in bits_bounds]


if __name__ == "__main__":
    bits = [8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
    keys = random_keys(bits)

    for i, key in enumerate(keys):
        print(f"Bruteforcing key {key}({bits[i]} bits)...")

        elapsed = bruteforce(key)

        print(f"Found key {key} in {elapsed} milliseconds")
        print()